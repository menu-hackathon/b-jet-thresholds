#!/usr/bin/env bash

set -eu

./run-swap.py > /tmp/swapped.txt
git diff --no-index --word-diff --word-diff-regex='[^_][0-9][^_]'  primary_chains.txt /tmp/swapped.txt
