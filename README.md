Trigger threshold swapperator
-----------------------------

The pt threshold remapping is in `pt.json`, other stuff is in `other.json`.

The pt remapping uses fancy regexes. The other stuff is just replaced.

You can look at the difference by running `show-diff.sh`.

List of thresholds was generated with

```
egrep -o '(_|presel)[0-9]?j[0-9]+' primary_chains.txt  | sed -r 's/(_|presel)[0-9]?j//'  | sort -n -u | awk 'BEGIN {print "{"} { printf("\"%s\":\"%i\",", $1, $1*0.94)} END {print "\"\":\"\"}"} ' | jq .
```

Should probably be looked over by someone.
