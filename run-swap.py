#!/usr/bin/env python3

"""
Fix some chains
"""

import json, re
from pathlib import Path
from argparse import ArgumentParser

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument(
        '-m',
        '--menu',
        type=Path
    )
    outputs = parser.add_mutually_exclusive_group()
    outputs.add_argument(
        '-r', '--raw-out-file',
        nargs='?',
        type=Path,
        const=Path('/dev/stdout'),
    )
    outputs.add_argument(
        '-o', '--out-menu',
        type=Path,
        nargs='?',
        const=Path('/dev/stdout'),
    )
    return parser.parse_args()

def run():
    args = get_args()
    chain_path = Path('primary_chains.txt')
    pt_path = Path('pt.json')
    other_path = Path('other.json')
    mods = get_modded(chain_path, pt_path, other_path)
    if args.menu:
        descending_length = sorted(mods, key=lambda x: len(x[0]), reverse=True)
        new_menu = []
        interleave = False
        if args.out_menu:
            out_path = args.out_menu
            interleave = True
        elif args.raw_out_file:
            out_path = args.raw_out_file
        else:
            out_path = Path('/dev/stdout')

        with open(args.menu,'r') as menu:
            for n, line in enumerate(menu):
                for old, new in descending_length:
                    if old == new:
                        continue
                    if old in line:
                        newline = line.replace(old, new)
                        new_menu.append(f'{newline.rstrip()} # downshift\n')
                        break
                # Print old (higher) threshold after new
                if interleave:
                    new_menu.append(line)
        with open(out_path, 'w') as menu:
            for line in new_menu:
                menu.write(line)

    for old, new in mods:
        print(new)

def get_modded(chain_path, pt_path, other_path):
    with open('primary_chains.txt') as chains_file:
        chains = [c.strip() for c in chains_file.readlines()]
    with open('other.json') as other_file:
        misc_replacements = json.load(other_file)
    # dumb way to prevent recursive replacement
    overlap = set(misc_replacements.keys()) & set(misc_replacements.values())
    assert not overlap, f'possible recursive replacement {overlap}'
    with open('pt.json') as replace_file:
        replacements = json.load(replace_file)
        def sub_string(match):
            return ''.join(
                [
                    match.group(1),
                    replacements[match.group(2)],
                ]
            )
    # look for a boi with some jet threshold, and with _pf_
    rep_re = re.compile('(_[0-9]*j)([0-9]+)(?=.*_pf_)')
    for chain in chains:
        if chain.strip().startswith("#") or not chain.strip():
            continue
        pt_modified = rep_re.sub(sub_string, chain)
        other_modified = pt_modified
        for old, new in misc_replacements.items():
            other_modified = other_modified.replace(old, new)
        yield chain, other_modified

if __name__ == '__main__':
    run()
